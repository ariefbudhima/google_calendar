package service

import (
	"fmt"
	"strings"
	"time"

	helpers "github.com/ariefbudhima/gcal/internal/app/helpers"
	d "github.com/ariefbudhima/gcal/internal/app/module/gcalendar/domain"
)

type GcalService struct {
	GcalRepo       d.GcalendarRepo
	contextTimeout time.Duration
}

func NewGcalService(gcalRepo d.GcalendarRepo, timeout time.Duration) d.GcalService {
	return &GcalService{
		GcalRepo:       gcalRepo,
		contextTimeout: timeout,
	}
}

func (gs GcalService) CreateGcal(i *d.GCalRequest) (*d.GCalRequest, error) {
	allEventId, err := helpers.GCalendarCreate(i)

	if err != nil {
		return nil, err
	}

	gcalSave := d.ChangeToGCalSave(*i)
	stringEventId := strings.Join(allEventId, " ")
	gcalSave.EventId = stringEventId
	fmt.Println(i)

	gcalnew, err := gs.GcalRepo.CreateGcal(&gcalSave)
	fmt.Println(err)
	i.Id = gcalnew

	return i, nil
}

func (gs GcalService) GetGcal(id int) (*d.GCalSave, error) {
	gcalsave, err := gs.GcalRepo.GetGcalbyId(id)

	return gcalsave, err
}

func (gs GcalService) UpdateGcal(id int, g *d.GcalEdit) (*d.GCalSave, error) {
	// in updategcal, we can only update date, time, and link location, not attendant, so we only need 3 parameter for this endpoint
	// get gcalsave and get event id for edit event
	gcalsave, err := gs.GcalRepo.GetGcalbyId(id)
	if err != nil {
		return nil, err
	}

	// update google calendar using helpers
	allEventId, err := helpers.GCalendarUpdate(gcalsave, g)
	if err != nil {
		return nil, err
	}
	// update gcalsave using data from helpers
	gcalsave.EventId = strings.Join(allEventId, " ")
	gcalsave.TimeSchedule = g.TimeSchedule
	gcalsave.DateSchedule = g.DateSchedule

	err = gs.GcalRepo.UpdateGcal(gcalsave)
	if err != nil {
		return nil, err
	}

	return gcalsave, nil
}

func (gs GcalService) DeleteGcal(id int) (*d.GCalSave, error) {
	// get from db, get event id
	gcaldb, err := gs.GcalRepo.GetGcalbyId(id)

	if err != nil {
		return nil, err
	}

	// var isExist bool

	allEventId := strings.Fields(gcaldb.EventId)
	fmt.Println(allEventId)
	fmt.Println("==================")
	fmt.Println(len(allEventId))

	// delete event using helpers
	fmt.Println("==============here serviced delete gcalendar==============")
	err = helpers.GcalendarDelete(allEventId)

	if err != nil {
		return nil, err
	}

	// delete database if event deleted in google calendar
	err = gs.GcalRepo.DeleteGcal(id)
	if err != nil {
		return nil, err
	}

	return gcaldb, nil
}
