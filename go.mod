module github.com/ariefbudhima/gcal

go 1.16

require (
	github.com/ekyoung/gin-nice-recovery v0.0.0-20160510022553-1654dca486db // indirect
	github.com/gin-contrib/cors v1.3.1 // indirect
	github.com/gin-gonic/gin v1.5.0 // indirect
	github.com/go-errors/errors v1.4.0 // indirect
	github.com/go-pg/pg v8.0.7+incompatible // indirect
	github.com/go-pg/pg/v10 v10.9.1 // indirect
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/lib/pq v1.6.0 // indirect
	github.com/swaggo/files v0.0.0-20190704085106-630677cd5c14 // indirect
	github.com/swaggo/gin-swagger v1.3.0 // indirect
	golang.org/x/oauth2 v0.0.0-20190226205417-e64efc72b421 // indirect
	google.golang.org/api v0.3.1 // indirect
	gorm.io/driver/postgres v1.1.0 // indirect
	gorm.io/gorm v1.21.10 // indirect
)
