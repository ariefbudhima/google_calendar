package main

import (
	"github.com/ariefbudhima/gcal/internal/app/delivery/http"
)

func main() {
	http.StartServer()
}
