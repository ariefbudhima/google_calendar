package helpers

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	d "github.com/ariefbudhima/gcal/internal/app/module/gcalendar/domain"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/calendar/v3"
)

func getClient(config *oauth2.Config) *http.Client {
	// The file token.json stores the user's access and refresh tokens, and is
	// created automatically when the authorization flow completes for the first
	// time.
	tokFile := "token.json"
	tok, err := tokenFromFile(tokFile)
	if err != nil {
		tok = getTokenFromWeb(config)
		saveToken(tokFile, tok)
	}
	httpClient := config.Client(context.Background(), tok)
	return httpClient
}

// Saves a token to a file path.
func saveToken(path string, token *oauth2.Token) {
	fmt.Printf("Saving credential file to: %s\n", path)
	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Fatalf("Unable to cache oauth token: %v", err)
	}
	defer f.Close()
	json.NewEncoder(f).Encode(token)
}

// Request a token from the web, then returns the retrieved token.
func getTokenFromWeb(config *oauth2.Config) *oauth2.Token {
	authURL := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
	fmt.Printf("Go to the following link in your browser then type the "+
		"authorization code: \n%v\n", authURL)

	var authCode string
	if _, err := fmt.Scan(&authCode); err != nil {
		log.Fatalf("Unable to read authorization code: %v", err)
	}

	tok, err := config.Exchange(context.TODO(), authCode)
	if err != nil {
		log.Fatalf("Unable to retrieve token from web: %v", err)
	}
	return tok
}

func tokenFromFile(file string) (*oauth2.Token, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	tok := &oauth2.Token{}
	err = json.NewDecoder(f).Decode(tok)
	return tok, err
}

// func Create(c *gin.Context) {
// 	var u GCalendarRequest
// 	// var p Participantss
// 	if err := c.ShouldBindJSON(&u); err != nil {
// 		Failed(c, errors.New("cannot decode"), http.StatusBadRequest)
// 		return
// 	}

// 	fmt.Println(len(u.Participants))
// 	fmt.Println(u.Participants)
// 	// Success(c, u, "", 0)
// 	tes, err := GCalendarCreate(&u)
// 	fmt.Println(tes)
// 	fmt.Println(err)
// 	Success(c, u.Participants[0].Name, "", 0)
// }

func GCalendarCreate(u *d.GCalRequest) ([]string, error) {
	srv, reminder, err := setup()
	fmt.Printf("create schedule on google calendar")
	fmt.Println(u)
	var allEventId []string

	eventP := &calendar.Event{
		Summary:     u.ServiceName,
		Location:    u.LinkLocation,
		Description: u.Description,
		Reminders:   reminder,
		Start: &calendar.EventDateTime{
			DateTime: "" + u.DateSchedule + "T" + u.TimeSchedule + "+07:00",
			TimeZone: "Asia/Jakarta",
		},
		End: &calendar.EventDateTime{
			DateTime: "" + u.DateSchedule + "T" + u.TimeSchedule + "+07:00",
			TimeZone: "Asia/Jakarta",
		}, Attendees: []*calendar.EventAttendee{
			&calendar.EventAttendee{Email: u.EmailOrganizer},
		},
	}

	eventsP, err := srv.Events.Insert("primary", eventP).SendUpdates("all").Do()
	if err != nil {
		fmt.Print("Unable to create event. because ")
		fmt.Println(err)
	} else {
		fmt.Printf("Event created:")
		allEventId = append(allEventId, eventsP.Id)
		fmt.Println("=========================event insert======================")
	}

	// looping here for every single participants
	for i := 0; i < len(u.Participants); i++ {
		eventU := &calendar.Event{
			Summary:     u.ServiceName,
			Location:    u.LinkLocation,
			Description: u.Description,
			Reminders:   reminder,
			Start: &calendar.EventDateTime{
				DateTime: "" + u.DateSchedule + "T" + u.TimeSchedule + "+07:00",
				TimeZone: "Asia/Jakarta",
			},
			End: &calendar.EventDateTime{
				DateTime: "" + u.DateSchedule + "T" + u.TimeSchedule + "+07:00",
				TimeZone: "Asia/Jakarta",
			}, Attendees: []*calendar.EventAttendee{
				&calendar.EventAttendee{Email: u.Participants[i]},
			},
		}

		eventsU, err := srv.Events.Insert("primary", eventU).SendUpdates("all").Do()
		if err != nil {
			fmt.Print("Unable to create event. because ")
			fmt.Println(err)
		} else {
			fmt.Printf("Event created:")
			allEventId = append(allEventId, eventsU.Id)
			fmt.Println("=========================event insert======================")

		}
		fmt.Println(&eventsU)
	}

	fmt.Println("=========event_id=========")
	fmt.Println(allEventId)
	return allEventId, err
}

func GCalendarUpdate(u *d.GCalSave, dt *d.GcalEdit) ([]string, error) {
	srv, reminder, err := setup()
	if err != nil {
		return nil, err
	}

	fmt.Printf("update schedule on google calendar")
	fmt.Println(u)
	// extract existing event id here
	exEventId := strings.Fields(u.EventId)
	fmt.Println("===================existing eventid")
	fmt.Println(exEventId)
	var allEventId []string
	participants := strings.Fields(u.Participants)

	eventP := &calendar.Event{
		Summary:     u.ServiceName,
		Location:    dt.LinkLocation,
		Description: u.Description,
		Reminders:   reminder,
		Start: &calendar.EventDateTime{
			DateTime: "" + dt.DateSchedule + "T" + dt.TimeSchedule + "+07:00",
			TimeZone: "Asia/Jakarta",
		},
		End: &calendar.EventDateTime{
			DateTime: "" + dt.DateSchedule + "T" + dt.TimeSchedule + "+07:00",
			TimeZone: "Asia/Jakarta",
		}, Attendees: []*calendar.EventAttendee{
			&calendar.EventAttendee{Email: u.EmailOrganizer},
		},
	}

	// eventsP, err := srv.Events.Insert("primary", eventP).SendUpdates("all").Do()
	eventsP, err := srv.Events.Update("primary", exEventId[0], eventP).SendUpdates("all").Do()

	if err != nil {
		fmt.Print("Unable to create event. because ")
		fmt.Println(err)
	} else {
		fmt.Printf("Event created:")
		allEventId = append(allEventId, eventsP.Id)
		fmt.Println("=========================event update======================")
	}

	// looping here for every single participants
	for i := 0; i < len(participants); i++ {
		eventU := &calendar.Event{
			Summary:     u.ServiceName,
			Location:    dt.LinkLocation,
			Description: u.Description,
			Reminders:   reminder,
			Start: &calendar.EventDateTime{
				DateTime: "" + dt.DateSchedule + "T" + dt.TimeSchedule + "+07:00",
				TimeZone: "Asia/Jakarta",
			},
			End: &calendar.EventDateTime{
				DateTime: "" + dt.DateSchedule + "T" + dt.TimeSchedule + "+07:00",
				TimeZone: "Asia/Jakarta",
			}, Attendees: []*calendar.EventAttendee{
				&calendar.EventAttendee{Email: participants[i]},
			},
		}

		eventsU, err := srv.Events.Update("primary", exEventId[i+1], eventU).SendUpdates("all").Do()

		if err != nil {
			fmt.Print("Unable to create event. because ")
			fmt.Println(err)
		} else {
			fmt.Printf("Event created:")
			allEventId = append(allEventId, eventsU.Id)
			fmt.Println("=========================event update======================")

		}
		// fmt.Println()
	}

	fmt.Println("=========event_id=========")
	fmt.Println(allEventId)
	return allEventId, err
}

func GcalendarDelete(eventid []string) error {
	srv, _, err := setup()

	if err != nil {
		return err
	}

	fmt.Println("=============inside of helper gcalendar delete==============")
	fmt.Println(len(eventid))

	for i := 0; i < len(eventid); i++ {
		err = srv.Events.Delete("primary", eventid[i]).SendUpdates("all").Do()
		if err != nil {
			fmt.Print("Unable to delete event. because ")
			fmt.Println(err)
		}
		fmt.Printf("Event deleted:")
		fmt.Println(i)
	}
	return nil
}

// func GCalendarUpdate()

func setup() (*calendar.Service, *calendar.EventReminders, error) {
	// Jalanin terlebih dahulu, taruh di main
	b, err := ioutil.ReadFile("./credentials.json")
	if err != nil {
		log.Fatalf("Unable to read client secret file: %v", err)
	}

	// If modifying these scopes, delete your previously saved token.json.
	config, err := google.ConfigFromJSON(b, calendar.CalendarEventsScope)
	if err != nil {
		log.Fatalf("Unable to parse client secret file to config: %v", err)
	}
	client := getClient(config)
	srv, err := calendar.New(client)
	if err != nil {
		log.Fatalf("Unable to retrieve Calendar client: %v", err)
	}

	// reminder 5jam dan 1jam sblm jadwal
	reminder := &calendar.EventReminders{
		Overrides: []*calendar.EventReminder{
			{
				Method:  "popup",
				Minutes: 300,
			}, {
				Method:  "popup",
				Minutes: 60,
			},
		},
		// UseDefault:      false,
		ForceSendFields: []string{"UseDefault"},
	}

	// sampai sini di main

	return srv, reminder, err
}

// func ChangeToGCalRequest(req *d.GCalRequest) *d.GCalRequest {
// 	var c *d.GCalRequest
// 	c.ServiceName = req.ServiceName
// 	c.LinkLocation = req.ServiceName
// 	c.EmailOrganizer = req.EmailOrganizer
// 	c.PsikologName = req.PsikologName
// 	c.Participants = req.Participants
// 	c.Description = req.Description
// 	c.DateSchedule = req.DateSchedule
// 	c.TimeSchedule = req.DateSchedule

// 	return c
// }
