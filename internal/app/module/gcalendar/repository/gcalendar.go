package repository

import (
	d "github.com/ariefbudhima/gcal/internal/app/module/gcalendar/domain"
	"gorm.io/gorm"
)

type repo struct {
	c *gorm.DB
}

func SetGcalendarRepo(c *gorm.DB) d.GcalendarRepo {
	return &repo{
		c: c,
	}
}

func (r *repo) CreateGcal(calendar *d.GCalSave) (int, error) {
	err := r.c.Create(calendar)

	return calendar.Id, err.Error
}

func (r *repo) GetGcalbyId(id int) (*d.GCalSave, error) {
	var gcal d.GCalSave
	err := r.c.Take(&gcal, id).Error

	return &gcal, err
}

func (r *repo) UpdateGcal(g *d.GCalSave) error {
	// var existGcal d.GCalSave
	// err := r.c.Table("google_calendar").Where("id = ?", id).Find(existGcal).Error
	err := r.c.Table("google_calendar").Where("id =?", g.Id).Updates(g)
	return err.Error
}

func (r *repo) DeleteGcal(id int) error {
	var cal d.GCalSave
	err := r.c.Delete(&cal, id)
	return err.Error
}

// add update and delete
