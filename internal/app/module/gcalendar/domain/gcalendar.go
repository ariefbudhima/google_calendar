package domain

import (
	"strings"
)

// type GCalendarRequest struct {
// 	ServiceName    string          `json:"service_name"`
// 	LinkLocation   string          `json:"link_location"`
// 	EmailOrganizer string          `json:"email_organizer"`
// 	PsikologName   string          `json:"psikolog_name"`
// 	Participants   []Participantss `json:"participants"`
// 	Description    string          `json:"description"`
// 	DateSchedule   string          `json:"date_schedule"`
// 	TimeSchedule   string          `json:"time_schedule"`
// }

type Participantss struct {
	Name  string `json:"name"`
	Email string `json:"email"`
}

// ini data yang disave
type CalendarEvent struct {
	Id      int    `json:"id,omitempty"`
	EventId string `json:"event_id,omitempty"`
	Name    string `json:"name,omitempty"`
	Phone   string `json:"phone,omitempty"`
}

type GCalRequest struct {
	Id             int      `json:"id,omitempty"`
	ServiceName    string   `json:"service_name"`
	LinkLocation   string   `json:"link_location"`
	EmailOrganizer string   `json:"email_organizer"`
	MeetingLeader  string   `json:"meeting_leader"`
	Participants   []string `json:"participants"`
	Description    string   `json:"description"`
	DateSchedule   string   `json:"date_schedule"`
	TimeSchedule   string   `json:"time_schedule"`
	EventId        string   `json:"event_id,omitempty" gorm:"type:text"`
}

type GCalSave struct {
	Id             int    `json:"id,omitempty"`
	ServiceName    string `json:"service_name"`
	LinkLocation   string `json:"link_location"`
	EmailOrganizer string `json:"email_organizer"`
	MeetingLeader  string `json:"meeting_leader"`
	Participants   string
	Description    string `json:"description"`
	DateSchedule   string `json:"date_schedule"`
	TimeSchedule   string `json:"time_schedule"`
	EventId        string `json:"event_id,omitempty" gorm:"type:text"`
}

type GcalEdit struct {
	LinkLocation string `json:"link_location"`
	DateSchedule string `json:"date_schedule"`
	TimeSchedule string `json:"time_schedule"`
}

type GcalendarRepo interface {
	CreateGcal(calendar *GCalSave) (int, error)
	GetGcalbyId(i int) (*GCalSave, error)
	UpdateGcal(i *GCalSave) error
	DeleteGcal(id int) error
}

type GcalService interface {
	CreateGcal(i *GCalRequest) (*GCalRequest, error)
	GetGcal(id int) (*GCalSave, error)
	UpdateGcal(id int, g *GcalEdit) (*GCalSave, error)
	DeleteGcal(id int) (*GCalSave, error)
}

func (g *GCalSave) TableName() string {
	return "google_calendar"
}

// half done

func ChangeToGCalSave(req GCalRequest) GCalSave {
	var c GCalSave
	c.ServiceName = req.ServiceName
	c.LinkLocation = req.ServiceName
	c.EmailOrganizer = req.EmailOrganizer
	c.MeetingLeader = req.MeetingLeader
	c.Participants = strings.Join(req.Participants, " ")
	c.Description = req.Description
	c.DateSchedule = req.DateSchedule
	c.TimeSchedule = req.DateSchedule

	return c
}
