package configuration

import (
	"fmt"
	"os"
)

func GetUriDatabase() string {
	fmt.Println("")
	return fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s",
		os.Getenv("DB_HOST"),
		os.Getenv("DB_PORT"),
		os.Getenv("DB_USER"),
		os.Getenv("DB_NAME"),
		os.Getenv("DB_PASSWORD"),
	)
}

func GetUser() string {
	return os.Getenv("DB_USER")
}

func GetPassword() string {
	return os.Getenv("DB_PASSWORD")
}

func GetAddress() string {
	return fmt.Sprintf("%s:%s", os.Getenv("DB_HOST"), os.Getenv("DB_PORT"))

}

func GetDBName() string {
	return os.Getenv("DB_NAME")
}

func GetAppPort() string {
	if os.Getenv("APP_PORT") == "" {
		panic("app port not set")
	} else {
		return os.Getenv("APP_PORT")
	}
}
