package http

import (
	"os"

	"github.com/joho/godotenv"

	conf "github.com/ariefbudhima/gcal/internal/app/configuration"
	"github.com/ariefbudhima/gcal/internal/app/helpers"
)

func StartServer() {
	// load .env
	_ = godotenv.Load()

	// set timezone
	_ = os.Setenv("TZ", "Asia/Jakarta")

	// init connection
	client := helpers.NewDBConn()

	// set route
	router := SetupRoute(client)

	// run router
	_ = router.Run(":" + conf.GetAppPort())

}
