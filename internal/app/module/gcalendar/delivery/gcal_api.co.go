// routing here
package delivery

import (
	"errors"
	"fmt"
	"strconv"

	"net/http"

	responses "github.com/ariefbudhima/gcal/internal/app/delivery/http/response"
	d "github.com/ariefbudhima/gcal/internal/app/module/gcalendar/domain"
	"github.com/gin-gonic/gin"
)

type GcalApi struct {
	GcalService d.GcalService
}

func NewGcalApi(route *gin.Engine, cs d.GcalService) {
	api := &GcalApi{
		GcalService: cs,
	}

	rg := route.Group("/gcal")
	{
		rg.GET("/", func(c *gin.Context) {
			responses.Success(c, nil, "ya gitu lah masuk ke gcal", 0)
		})
		rg.POST("/create", api.Create)
		rg.PUT("/:id", api.Update)
		rg.DELETE("/:id", api.Delete)
		rg.GET("/:id", api.GetGcal)
	}
}

// di sini dibuat routing, mau ke mana, ke controller (service) mana
func (api *GcalApi) Create(c *gin.Context) {
	var u d.GCalRequest
	if err := c.ShouldBindJSON(&u); err != nil {
		responses.Failed(c, errors.New("cannot decode"), http.StatusBadRequest)
		return
	}

	result, err := api.GcalService.CreateGcal(&u)
	if err != nil {
		responses.Failed(c, err, 410)
		return
	}
	fmt.Println(err)

	responses.Success(c, result, "", 0)
	fmt.Println("=====================================")

}

func (api *GcalApi) Delete(c *gin.Context) {
	strid := c.Param("id")
	id, _ := strconv.Atoi(strid)

	getGcalendar, err := api.GcalService.DeleteGcal(id)

	if err != nil {
		responses.Failed(c, err, 410)
		return
	}
	responses.Success(c, getGcalendar, "", 0)
	return

}

func (api *GcalApi) GetGcal(c *gin.Context) {
	// var id int
	strid := c.Param("id")
	id, _ := strconv.Atoi(strid)

	var gcal *d.GCalSave

	gcal, err := api.GcalService.GetGcal(id)

	if err != nil {
		responses.Failed(c, err, 410)
		return
	}

	responses.Success(c, gcal, "", 0)
}

func (api *GcalApi) Update(c *gin.Context) {
	strid := c.Param("id")
	id, _ := strconv.Atoi(strid)

	var u d.GcalEdit
	if err := c.ShouldBindJSON(&u); err != nil {
		responses.Failed(c, errors.New("cannot decode"), http.StatusBadRequest)
		return
	}

	gcal, err := api.GcalService.UpdateGcal(id, &u)

	if err != nil {
		responses.Failed(c, err, 410)
		return
	}
	responses.Success(c, gcal, "", 0)

	fmt.Println(id, gcal)
}
