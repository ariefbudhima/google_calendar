package http

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"time"

	nice "github.com/ekyoung/gin-nice-recovery"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	conf "github.com/ariefbudhima/gcal/internal/app/configuration"

	res "github.com/ariefbudhima/gcal/internal/app/delivery/http/response"
	gcalD "github.com/ariefbudhima/gcal/internal/app/module/gcalendar/delivery"
	gcalR "github.com/ariefbudhima/gcal/internal/app/module/gcalendar/repository"
	gcalS "github.com/ariefbudhima/gcal/internal/app/module/gcalendar/service"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

var timeout time.Duration = 5

func SetupRoute(client *gorm.DB) *gin.Engine {
	gin.ForceConsoleColor()
	f, _ := os.Create("gin.log")
	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)

	router := gin.New()
	router.Use(gin.LoggerWithFormatter(func(param gin.LogFormatterParams) string {
		return fmt.Sprintf("%s - [%s] \"%s %s \n %s %s \n\n",
			param.ClientIP,
			param.TimeStamp.Format(time.RFC822),
			param.Method,
			param.Request.URL,
			param.Request.Header,
			param.ErrorMessage,
		)
	}))

	// Recovery middleware recovers from any panics and writes a 500 if there was one.
	router.Use(nice.Recovery(func(c *gin.Context, err interface{}) {
		res.Failed(c, errors.New("an error was encountered. please come back later"), 0)
	}))

	// index
	router.GET("/", func(c *gin.Context) {
		res.Success(c, nil, "endpoint payment element alive", 0)
	})

	// no route handler
	router.NoRoute(func(c *gin.Context) {
		res.Failed(c, errors.New("page not found"), http.StatusNotFound)
	})

	// swagger
	router.GET("/docs/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	// setup module
	SetupModule(router, client)

	return router
}

func SetupModule(router *gin.Engine, client *gorm.DB) {
	// put here service
	gcalService := gcalS.NewGcalService(
		gcalR.SetGcalendarRepo(client),
		timeout,
	)

	router.Use(conf.GetCors())
	{
		// put here delivery or controller
		gcalD.NewGcalApi(router, gcalService)
	}
}
