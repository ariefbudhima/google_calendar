package helpers

import (
	conf "github.com/ariefbudhima/gcal/internal/app/configuration"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var db *gorm.DB
var err error

func NewDBConn() *gorm.DB {
	if db, err = gorm.Open(postgres.Open(conf.GetUriDatabase()), &gorm.Config{}); err != nil {
		panic(err)
	}

	return db
}
